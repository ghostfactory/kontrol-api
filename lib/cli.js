/**
 * Execute a CLI command.
 * Manage Windows and Unix environment and try to execute the command on both env if fails.
 * Order: Windows -> Unix.
 *
 * @param command                   Command to execute. ('grunt')
 * @param args                      Args of the command. ('watch')
 * @param callback                  Success.
 * @param callbackErrorWindows      Failure on Windows env.
 * @param callbackErrorUnix         Failure on Unix env.
 */
exports.execute = function (command, args, callback, callbackErrorWindows, callbackErrorUnix) {
    if (typeof args === "undefined") {
        args = [];
    }
    exports.windows(command, args, callback, function () {
        if(callbackErrorWindows) {
            callbackErrorWindows();
        }

        try {
            exports.unix(command, args, callback, callbackErrorUnix);
        } catch (e) {
            console.log('------------- Failed to perform the command: "' + command + '" on all environments. -------------');
        }
    });
};

/**
 * Execute a command on Windows environment.
 *
 * @param command       Command to execute. ('grunt')
 * @param args          Args of the command. ('watch')
 * @param callback      Success callback.
 * @param callbackError Failure callback.
 */
exports.windows = function(command, args, callback, callbackError) {
    if (typeof args === "undefined") {
        args = [];
    }
    try {
        exports._execute(process.env.comspec, _.union(['/c', command], args));
        if(callback) {
            callback(command, args, 'Windows');
        }
    } catch (e) {
        if(callbackError) {
            callbackError(command, args, 'Windows');
        }
    }
};

/**
 * Execute a command on Unix environment.
 *
 * @param command       Command to execute. ('grunt')
 * @param args          Args of the command. ('watch')
 * @param callback      Success callback.
 * @param callbackError Failure callback.
 */
exports.unix = function (command, args, callback, callbackError) {
    if (typeof args === "undefined") {
        args = [];
    }
    try {
        exports._execute(command, args);
        if(callback) {
            callback(command, args, 'Unix');
        }
    } catch (e) {
        if(callbackError) {
            callbackError(command, args, 'Unix');
        }
    }
};

/**
 * Execute a command no matters what's the environment.
 *
 * @param command   Command to execute. ('grunt')
 * @param args      Args of the command. ('watch')
 * @private
 */
exports._execute = function (command, args) {
    var spawn = require('child_process').exec;
    var childProcess = spawn(command, args);

    childProcess.stdout.on("data", function (data) {
        console.log(data.toString());
    });

    childProcess.stderr.on("data", function (data) {
        console.error(data.toString());
    });
};