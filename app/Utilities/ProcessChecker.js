/* ==== Libs ==== */

var _ = require("underscore");

/* ============================================================ */


module.exports = function()
{
    var _this = this;

    _this.getProcesses = function(processName)
    {
        var output = exec('ps axco pid,command | grep ' + processName, {silent: true}).output;
        var lines = output.split("\n");
        lines = _.without(lines, "");

        return _.map(lines, function(line) {
            var parts = line.split(" ");
            var pid = parts[0];
            var name = _.rest(parts).join(" ");
            return {
                pid: pid,
                name: name
            }
        });
    }

    _this.isSimilarProcessRunning = function(processName)
    {
        var processes = _this.getProcesses(processName);

        return processes.length === 0 ? false : true;
    }

    _this.isExactProcessRunning = function(processName)
    {
        var processes = _this.getProcesses(processName);

        var find = _.find(processes, function(process) {
            return process.name === processName;
        });

        return typeof find === 'undefined' ? false : true;
    }

}
