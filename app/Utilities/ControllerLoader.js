module.exports = function(server, io)
{
    var _this = this;

    _this.server = server;
    _this.io = io;

    _this.load = function()
    {
        require(process.cwd() + '/bundles/Mopidy/MopidyController')(_this.server, _this.io);
        require(process.cwd() + '/bundles/DigitalOcean/DigitalOceanController')(_this.server, _this.io);
        require(process.cwd() + '/bundles/CatBot/CatBotController')(_this.server, _this.io);
        require(process.cwd() + '/bundles/Speak/SpeakController')(_this.server, _this.io);
        //require(process.cwd() + '/app/raspi/RaspiController')(server, io);
    }

    /* Something else */
    //server.post("/programs", checkDB, controllers.program.createProcess);
    //server.put("/programs/:id", checkDB, controllers.program.updateProcess);
    //server.del("/programs/:id", controllers.program.deleteProcess);
    //server.get({path: "/programs/:id", version: "1.0.0"}, controllers.program.viewProcess);
    //server.get({path: "/programs/:id", version: "2.0.0"}, controllers.program.viewProcess_v2);

}
