module.exports = function(response, data, code)
{
    var _this = this;

    _this.response = response;
    _this.data = data;
    _this.code = code;

    _this.success = function()
    {
        if(response.req.params.callback) {
            _this.data = JSON.parse("JSON_CALLBACK(" + data + ")");
        }
        return _this.response.json(_this.code || 200, _this.data);
    }

    _this.fail = function()
    {
        return _this.response.json(_this.code || 400, {
            status: "fail",
            data: missingParamsData(_this.data)
        });
    }

    _this.failWithMessage = function(message)
    {
        return _this.response.json(_this.code || 400, {
            status: "fail",
            message: message,
            data: missingParamsData(_this.data)
        });
    }

    _this.error = function(message, error)
    {
        return _this.response.json(_this.code || 500, {
            status: "error",
            message: message,
            error: error
        });
    }

    function missingParamsData(missingParams)
    {
        var data = {};
        for(var i in missingParams) {
            var missingParam = missingParams[i];
            data[missingParam] = "Missing required parameter: " + missingParam;
        }
        return data;
    }
}