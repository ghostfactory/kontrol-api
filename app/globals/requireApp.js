global.requireApp = function(path)
{
    if(path.indexOf("/") == 0) {
        path = path.substring(1);
    }
    if(path.indexOf(".js") == -1) {
        path = path + ".js";
    }

    return require(process.cwd() + "/" + path);
}