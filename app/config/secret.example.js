module.exports = {
    ssl: {
        key: "PATH_TO_SSL_KEY",
        certificate: "PATH_TO_SSL_CERTIFICATE"
    },
    digitalOcean: {
        key: "DIGITAL_OCEAN_API_KEY"
    }
}