var SECRET = requireApp("app/config/secret");

module.exports = {
    app: {
        name: 'kontrol-api',
        port: '3000'
    },
    ssl: {
        key: SECRET.ssl.key,
        certificate: SECRET.ssl.certificate
    },
    digitalOcean: {
        key: SECRET.digitalOcean.key,
        perPage: 50
    },
    catBot: {
        processName: "catbotfuntime"
    }
};
