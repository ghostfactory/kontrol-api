Kontrol API
============

*This program is currently not set up to be easily cloned an ran, as there are some hard-coded values specific to my setup, specifically Raspberry Pi GPIO pins, and multiple required programs yet to be fully listed. If you decide to play around with the program, some tinkering will be needed to make it work correctly. I will try to document everything when I can.*

Kontrol is a work-in-progress suite of applications that provide multiple different home-automation services. Kontrol API is currently built to run on a Raspberry Pi and provides access for clients to control different home-automation-y things.

Current Services
------------------------
* Spotify (via [Mopidy](https://www.mopidy.com/) and the [Mopidy Node Package](https://www.npmjs.com/package/mopidy))
* Text-to-Speech
* Super basic LED control and temperature sensing