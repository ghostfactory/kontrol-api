/* ==== Globals ==== */

require("./app/globals/requireApp");
require('shelljs/global');

/* ==== Imports ==== */

var Restify = require("restify");
var SocketIO = require("socket.io");

/* ==== App ==== */

var CONFIG = requireApp("app/config/config");
var ControllerLoader = requireApp("app/Utilities/ControllerLoader");

/* ==== Libs ==== */

var fs = require("fs");
var util = require("./lib/util");
var cli = require("./lib/cli");


/* ============================================================ */

process.title = CONFIG.app.name;

// Server
var serverConfig = {name: CONFIG.app.name};

if(fs.existsSync(CONFIG.ssl.key) && fs.existsSync(CONFIG.ssl.certificate)) {
    console.log("Using SSL.")
    serverConfig.key = fs.readFileSync(CONFIG.ssl.key);
    serverConfig.certificate = fs.readFileSync(CONFIG.ssl.certificate);
//    CONFIG.app.port = 443;
}

var server = Restify.createServer(serverConfig);
server.use(Restify.fullResponse()).use(Restify.bodyParser()).use(Restify.queryParser());

// Socket.io
var io = SocketIO.listen(server.server);

// Controllers
var controllerLoader = new ControllerLoader(server, io);
controllerLoader.load();

// Start server
server.listen(CONFIG.app.port, function (error) {
    if (error) {
        return console.error(error);
    }

    console.log("%s listening at %s", server.name, server.url);
});

// Production error handling
if(process.env.environment == "production") {
    process.on("uncaughtException", function (error) {
        console.error(JSON.parse(JSON.stringify(error, ["stack", "message", "inner"], 2)));
    });
}
