var CLI = require(process.cwd() + '/lib/cli');

/* =========================================== */

exports.config = {
    name: 'Speak'
};

exports.paths = {
    speakScript: process.cwd() + '/app/speak/speak.sh'
}

/* =========================================== */

exports.speak = function(text, successCallback, errorCallback) {
    var script = exports.paths.speakScript + ' ' + text;
    return CLI.unix(script, null, successCallback, errorCallback);
}