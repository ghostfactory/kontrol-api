/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");
var SpeakProgram = requireApp('bundles/Speak/SpeakProgram');

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(server, io)
{

    /* ================================================= */
    /* POST
    /* ================================================= */

    /*
     * Speak Word
     */
    server.post("/speak", function (request, response, next) {
        if(Util.validationResponse(['text'], request, response)) return;
        var params = {
            text: Util.params(request.params.text)
        };

        SpeakProgram.speak(params.text, function (data) {
            return Response.success(response, data);
        }, function (error) {
            return Response.error(response, "There was an error speaking.", error);
        });

    });
}