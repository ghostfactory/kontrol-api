/* ==== App ==== */

var CONFIG = requireApp("app/config/config");
var DigitalOcean = require('do-wrapper');


/* ============================================================ */

module.exports = function(server, io) {

    var digitalOcean = new DigitalOcean(CONFIG.digitalOcean.key, CONFIG.digitalOcean.perPage);

    require('./Controllers/AccountController')(digitalOcean, server, io);
    require('./Controllers/DomainController')(digitalOcean, server, io);
    require('./Controllers/DropletController')(digitalOcean, server, io);

}