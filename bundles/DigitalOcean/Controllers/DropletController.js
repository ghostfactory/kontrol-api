/* ==== App ==== */

var CONFIG = requireApp("app/config/config");
var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(digitalOcean, server, io)
{
    var resource = '/digitalocean/droplets';


    /* ================================================= */
    /* POST
     /* ================================================= */




    /* ================================================= */
    /* GET
     /* ================================================= */

    /*
     * Droplets
     */
    server.get(resource, function (request, response, next) {
        digitalOcean.dropletsGetAll(CONFIG.digitalOcean.perPage, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting droplets.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Single Droplet
     */
    server.get(resource + "/:id", function (request, response, next) {
        var id = Util.params(request.params.id);

        digitalOcean.dropletsGetById(id, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting droplet.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Droplet Snapshots
     */
    server.get(resource + "/:id/snapshots", function (request, response, next) {
        var id = Util.params(request.params.id);

        digitalOcean.dropletsGetSnapshots(id, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting droplet snapshots.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Droplet Backups
     */
    server.get(resource + "/:id/backups", function (request, response, next) {
        var id = Util.params(request.params.id);

        digitalOcean.dropletsGetBackups(id, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting droplet backups.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Droplet Actions
     */
    server.get(resource + "/:id/actions", function (request, response, next) {
        var id = Util.params(request.params.id);

        digitalOcean.dropletsGetActions(id, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting droplet actions.", err);
            }

            return new Response(response, body).success();
        });
    });

}