/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(digitalOcean, server, io)
{
    var resource = '/digitalocean/domains';


    /* ================================================= */
    /* POST
    /* ================================================= */




    /* ================================================= */
    /* GET
     /* ================================================= */

    /*
     * Domains
     */
    server.get(resource, function (request, response, next) {
        digitalOcean.domainsGetAll(CONFIG.digitalOcean.perPage, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting domains.", err);
            }

            return new Response(response, body).success();
        });
    });

}