/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(digitalOcean, server, io)
{
    var resource = '/digitalocean/account';

    /* ================================================= */
    /* POST
    /* ================================================= */




    /* ================================================= */
    /* GET
     /* ================================================= */

    /*
     * Account Info
     */
    server.get(resource, function (request, response, next) {
        digitalOcean.account(function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting account details.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Actions
     */
    server.get(resource + "/actions", function (request, response, next) {
        digitalOcean.accountGetActions(CONFIG.digitalOcean.perPage, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting actions.", err);
            }

            return new Response(response, body).success();
        });
    });

    /*
     * Single Action
     */
    server.get(resource + "/actions/:id", function (request, response, next) {
        var id = Util.params(request.params.id);

        digitalOcean.accountGetAction(id, function (err, doRes, body) {
            if(err) {
                return new Response(response).error("There was an error getting action.", err);
            }

            return new Response(response, body).success();
        });
    });

}