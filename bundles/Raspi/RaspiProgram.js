var Gpio = require('onoff').Gpio;

/* =========================================== */

var led = {
    red: new Gpio(17, 'out'),
    green: new Gpio(27, 'out'),
    blue: new Gpio(22, 'out')
}

exports.config = {
    name: 'Raspi'
};

exports.paths = {

}

/* =========================================== */

exports.light = function(successCallback, errorCallback) {
	try {
		for(var color in led) {
        		var value = led[color].readSync() == 0 ? 1 : 0;
        		led[color].writeSync(value);
    		}
	} catch(error) {
		errorCallback(error);
	}
	successCallback();
}

function randOnOff() {
    return Math.round(Math.random());
}
