var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');

var RaspiProgram = require('./raspiProgram');

/* =========================================== */

module.exports = function(server, io) {
    var resource = '/raspi';

    /* ================================================= */
    /* POST
    /* ================================================= */

    /*
     * Light
     */
    server.post(resource + "/light", function (request, response, next) {
        RaspiProgram.light(function (data) {
            return Response.success(response, data);
        }, function (error) {
            return Response.error(response, "There was an error toggling light.", error);
        });

    });
}