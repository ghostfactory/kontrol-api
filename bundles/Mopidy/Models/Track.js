var Album = require('./Album');
var Artist = require('./Artist');

module.exports = function(data)
{
    if(data == null) {
        return null;
    }

    var _this = this;

    _this.name = data.name;
    _this.uri = data.uri;
    _this.length = data.length;
    _this.lengthString = "";
    _this.trackNum = data.track_no;
    _this.date = data.date;
    _this.bitrate = data.bitrate;
    _this.album = new Album(data.album);
    _this.artists = [];
    _this.artistsString = "";
    _this.timePosition = data.timePosition || 0;
    _this.timePercent = (_this.timePosition / _this.length) * 100;

    // Set artists & artist string
    for(var i in data.artists) {
        var artistData = data.artists[i];
        var artist = new Artist(artistData);
        _this.artists.push(artist);

        _this.artistsString += artist.name;
        if(i < data.artists.length - 1) {
            _this.artistsString += ", ";
        }
    }

    // Set lengthString
    var minutes = Math.floor((_this.length / 1000) / 60);
    var seconds = (_this.length / 1000) % 60;
    _this.lengthString = minutes + ":" + seconds;

    // From Playlist model
    // TODO: Are these still needed?
    _this.playlistUri = null;
    _this.playlistNum = null;
    _this.playlistCount = null;

    return _this;
}