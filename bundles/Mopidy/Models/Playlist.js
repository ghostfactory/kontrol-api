var Track = require('./Track');

module.exports = function(data)
{
    if(data == null) {
        return null;
    }

    var _this = this;

    _this.name = data.name;
    _this.fullName = "";
    _this.foldersString = "";
    _this.uri = data.uri;
    _this.uriUrl = encodeURIComponent(data.uri);
    _this.folders = [];
    _this.tracks = [];

    // Set tracks
    for(var i in data.tracks) {
        var trackData = data.tracks[i];
        var track = new Track(trackData);

        // TODO: Are these still needed? Or is this best place?
        track.playlistUri = _this.uri;
        track.playlistNum = parseInt(i);
        track.playlistCount = data.tracks.length;

        _this.tracks.push(track);
    }

    // Get folder parts
    var folderParts = _this.name.split("/");
    for(var i in folderParts) {
        var part = folderParts[i];

        if(i < folderParts.length - 1) {
            _this.folders.push(part);
        } else {
            _this.name = part;
        }

        _this.fullName += (i == folderParts.length - 1) ? part : "[" + part + "] ";
    }

    // Set foldersName
    if(_this.folders.length > 0) {
        _this.foldersString = _this.folders.join(" / ");
    }

    return _this;
}