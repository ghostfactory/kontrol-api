var Artist = require('./Artist');

module.exports = function(data)
{
    if(data == null) {
        return null;
    }

    var _this = this;

    _this.name = data.name;
    _this.uri = data.uri;
    _this.date = data.date;
    _this.artists = [];

    // Set artists
    for(var i in data.artists) {
        var artistData = data.artists[i];
        var artist = new Artist(artistData);
        _this.artists.push(artist);
    }

    return _this;
}