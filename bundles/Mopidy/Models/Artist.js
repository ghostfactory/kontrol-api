module.exports = function(data)
{
    if(data == null) {
        return null;
    }

    var _this = this;

    _this.name = data.name;
    _this.uri = data.uri;

    return _this;
}