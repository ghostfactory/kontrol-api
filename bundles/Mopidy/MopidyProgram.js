/* ==== App ==== */

var Playlist = requireApp('bundles/Mopidy/Models/Playlist');
var Track = requireApp('bundles/Mopidy/Models/Track');

/* ==== Imports ==== */

var Mopidy = require("mopidy");

/* =========================================== */


module.exports = function(io) {
    var program = {};

    var mopidy = new Mopidy({
        webSocketUrl: "ws://localhost:6680/mopidy/ws/",
        callingConvention: "by-position-or-by-name"
    });

    mopidy.isReady = false;
    mopidy.notReady = function (errorCallback) {
        return errorCallback("Mopidy is not ready yet!");
    }

    program.tracklist = mopidy.tracklistManager = require('./Managers/TracklistManager')(mopidy, io);
    program.playlists = mopidy.playlistsManager = require('./Managers/PlaylistsManager')(mopidy, io);
    program.playback = mopidy.playbackManager = require('./Managers/PlaybackManager')(mopidy, io);
    program.library = mopidy.libraryManager = require('./Managers/LibraryManager')(mopidy, io);
    program.core = mopidy.coreManager = require('./Managers/CoreManager')(mopidy, io);


    /* ============================================= */
    /* Events
    /* ============================================= */
    mopidy.on("state:online", function () {
        mopidy.isReady = true;
        console.log("===== READY =====");
    });

    mopidy.on(function (event) {
        console.log(event);
    });

    /* State changed */
    mopidy.on("event:playbackStateChanged", function(data) {
        io.emit("mopidy.playbackStateChanged", data);

        //mopidy.playbackManager.getCurrentTlTrack(function(tl_track){
        //    if(data.new_state === 'stopped') {
        //        data.tl_track = null;
        //    } else {
        //        data.tl_track = tl_track;
        //    }
        //    io.emit("event:playbackStateChanged", data);
        //}, function(error) {
        //    io.emit("error:playbackStateChanged", error);
        //});
    });

    /* Playback started */
    mopidy.on("event:trackPlaybackStarted", function(data) {
        var track = new Track(data.tl_track.track);
        io.emit("mopidy.playbackStarted", track);
    });

    /* Playback paused */
    mopidy.on("event:trackPlaybackPaused", function(data) {
        io.emit("mopidy.playbackPaused", data);
    });

    /* Playback resumed */
    mopidy.on("event:trackPlaybackResumed", function(data) {
        io.emit("mopidy.playbackResumed", data);
    });

    /* Playback ended */
    mopidy.on("event:trackPlaybackEnded", function(data) {
        io.emit("mopidy.playbackEnded", data);
    });

    /* Tracklist changed */
    mopidy.on("event:tracklistChanged", function(data) {
        mopidy.tracklistManager.getTlTracks(function(tl_tracks){
            data.tl_tracks = tl_tracks;
            io.emit("mopidy.tracklistChanged", data);
        }, function(error) {
            io.emit("mopidy.tracklistChanged", error);
        });
    });

    return program;
}