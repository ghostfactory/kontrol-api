/* ==== App ==== */

var MopidyProgram = requireApp('bundles/Mopidy/MopidyProgram');


/* ============================================================ */

module.exports = function(server, io) {

    var mopidyProgram = new MopidyProgram(io);

    require('./Controllers/PlaylistsController')(mopidyProgram, server, io);
    require('./Controllers/TracklistController')(mopidyProgram, server, io);
    require('./Controllers/PlaybackController')(mopidyProgram, server, io);
    require('./Controllers/LibraryController')(mopidyProgram, server, io);
    require('./Controllers/CoreController')(mopidyProgram, server, io);

}