/* ==== App ==== */

var Track = requireApp('bundles/Mopidy/Models/Track');

/* ==== Libs ==== */

_ = require('underscore');

/* =========================================== */


module.exports = function(mopidy, io) {
    var manager = {};

    /**
     * Play Tracklist Track
     * @param {object|null} tl_track tracklist track object
     * @param {int|null} on_error_step either 1 or -1
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playTlTrack = function(tl_track, on_error_step, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        if(on_error_step != 1 && on_error_step != -1){
            on_error_step = 1;
        }

        return mopidy.playback.play({tl_track: tl_track/*, on_error_step: on_error_step*/}).then(function(data) {
            manager.getCurrentTrack(function(track) {
                io.emit('mopidy.playback.play', track);
                successCallback(track);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Play URI
     * @param {string} uri URI of playlist, track, artist, etc.
     * @param {int|null} at_position track index to start on
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playUri = function(uri, at_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        // Set tracklist to URI
        return mopidy.tracklistManager.setUri(uri, function(tracklist) {
            // Get the next track in the tracklist
            return mopidy.tracklistManager.getNextTlTrack(null, function(tl_track) {
                // If at_position set, overwrite next track
                if(at_position && at_position > 0 && at_position <= tracklist.length) {
                    tl_track = tracklist[at_position];
                }

                // Check if already playing
                mopidy.playback.getCurrentTrack().then(function(track) {
                    // Already playing
                    if(track != null && track.uri == tl_track.track.uri) {
                        successCallback(track);
                        return;
                    }

                    // Start playing
                    return manager.playTlTrack(tl_track, null, function(track) {
                        io.emit('mopidy.playback.play', track);
                        successCallback(track);
                    }, errorCallback);
                }, errorCallback);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Play URI in playlist
     * @param {string} playlistUri URI of playlist
     * @param {int|null} trackUri URI of the track in the playlist
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playUriInPlaylist = function(playlistUri, trackUri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklistManager.setUri(playlistUri, function(tracklist) {
            var at_position = _.findIndex(tracklist, function(t) {
                return t.track.uri == trackUri;
            });

            manager.playUri(playlistUri, at_position, successCallback, errorCallback);
        }, errorCallback);
    }

    /**
     * Play Tracks
     * @param {array} tracks list of tracks
     * @param {int|null} at_position track index to start on
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playTracks = function(tracks, at_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklistManager.setTracks(tracks, function(tracklist) {
            if(at_position && at_position > 0 && at_position <= tracklist.length) {
                var playTrack = at_position;
                return manager.playTlTrack(tracklist[playTrack], null, function(data) {
                    io.emit('mopidy.playback.play', data);
                    successCallback(data);
                }, errorCallback);
            }

            return mopidy.tracklistManager.getNextTlTrack(null, function(tl_track) {
                return manager.playTlTrack(tl_track, null, function(data) {
                    io.emit('mopidy.playback.play', data);
                    successCallback(data);
                }, errorCallback);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Set the time position
     * TODO: NOT WORKING :( SWITCHES SONGS
     * @param {int} time_position in milliseconds
     * @param {function} successCallback returns true if success
     * @param {function} errorCallback returns the error
     */
    manager.seek = function(time_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.seek({time_position: time_position}).then(function(data) {
            io.emit('mopidy.playback.seek', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get the time position
     * @param {function} successCallback returns the time position in milliseconds
     * @param {function} errorCallback returns the error
     */
    manager.getTimePosition = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getTimePosition().then(successCallback, errorCallback);
    }

    /**
     * Stop playing
     * @param {boolean|null} clear_current_track whether to clear the current playing track after stopping
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.stop = function(clear_current_track, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        if(clear_current_track == null) {
            clear_current_track = false;
        }

        return mopidy.playback.stop({clear_current_track: clear_current_track}).then(function(data) {
            io.emit('mopidy.playback.stop', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Pause playing
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.pause = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.pause().then(function(data) {
            io.emit('mopidy.playback.pause', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Resume playing if paused
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.resume = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.resume().then(function(data) {
            io.emit('mopidy.playback.resume', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get current player state
     * @param {function} successCallback returns 'paused', 'playing', or 'stopped'
     * @param {function} errorCallback returns the error
     */
    manager.getState = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getState().then(successCallback, errorCallback);
    }

    /**
     * Change tracklist track
     * @param {object} tl_track tracklist track object
     * @param {int|null} on_error_step either 1 or -1
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.changeTlTrack = function(tl_track, on_error_step, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        if(on_error_step == null) {
            on_error_step = 1;
        }

        return mopidy.playback.changeTrack({tl_track: tl_track, on_error_step: on_error_step}).then(function(data) {
            manager.getCurrentTrack(function(track) {
                io.emit('mopidy.playback.play', track);
                successCallback(track);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Change URI
     * @param {string} uri URI of playlist, track, artist, etc.
     *  Should be a URI in the tracklist.
     * @param {int|null} on_error_step either 1 or -1
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.changeUri = function(uri, on_error_step, successCallback, errorCallback) {
        return mopidy.tracklistManager.getTlTracks(function(tracklist) {
            var tl_track = null;
            for(var i in tracklist) {
                var checkUri = tracklist[i].track.uri;
                if(uri === checkUri) {
                    tl_track = tracklist[i];
                    break;
                }
            }

            if(tl_track === null) {
                return errorCallback("URI not in tracklist.");
            }

            return manager.changeTlTrack(tl_track, on_error_step, function(data) {
                io.emit('mopidy.playback.play', data);
                successCallback(data);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Play next track
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playNextTrack = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.next().then(function(data) {
            io.emit('mopidy.playback.play', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Play previous track
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.playPreviousTrack = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.previous().then(function(data) {
            io.emit('mopidy.playback.play', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Set mute
     * TODO: SETTING MUTE OFF DOES NOT WORK??
     * @param {boolean} mute turn mute on or off
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.setMute = function(mute, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mute = (mute == true ? true : false);

        return mopidy.playback.setMute([mute]).then(function(data) {
            io.emit('mopidy.playback.mute', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get mute
     * @param {function} successCallback returns boolean
     * @param {function} errorCallback returns the error
     */
    manager.getMute = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getMute().then(successCallback, errorCallback);
    }

    /**
     * Get volume
     * @param {function} successCallback returns int in range [1..100] or null if unknown
     * @param {function} errorCallback returns the error
     */
    manager.getVolume = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getVolume().then(successCallback, errorCallback);
    }

    /**
     * Get current playing track
     * @param {function} successCallback returns track object
     * @param {function} errorCallback returns the error
     */
    manager.getCurrentTrack = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getCurrentTrack().then(function(trackData) {
            if(trackData == null) {
                successCallback(null);
                return;
            }

            // Get time position
            manager.getTimePosition(function (timePosition) {
                trackData.timePosition = timePosition;
                var track = new Track(trackData);

                successCallback(track);
            }, errorCallback);
        }, errorCallback);
    };

    /**
     * Get current playing tracklist track
     * @param {function} successCallback returns tl_track object
     * @param {function} errorCallback returns the error
     */
    manager.getCurrentTlTrack = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.playback.getCurrentTlTrack().then(successCallback, errorCallback);
    };

    return manager;
}