
module.exports = function(mopidy, io) {
    var manager = {};

    /**
     * Add tracks
     * Triggers CoreListener.tracklist_changed().
     * @param {array} tracks list of track objects
     * @param {int|null} at_position where to insert tracks
     * @param {function} successCallback returns array of added tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.addTracks = function(tracks, at_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.add({tracks: tracks, at_position: at_position}).then(function(data) {
            io.emit('mopidy.tracklist.add', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Add tracks from URI
     * Triggers CoreListener.tracklist_changed().
     * @param {string} uri the URI of the track
     * @param {int|null} at_position where to insert tracks
     * @param {function} successCallback returns array of added tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.addUri = function(uri, at_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.add({uri: uri, at_position: at_position}).then(function(data) {
            io.emit('mopidy.tracklist.add', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Set tracks, clearing previous tracks
     * Triggers CoreListener.tracklist_changed().
     * @param {array} tracks list of track objects
     * @param {function} successCallback returns array of tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.setTracks = function(tracks, successCallback, errorCallback) {
        return manager.clear(function(data) {
            return manager.addTracks(tracks, null, function(data) {
                io.emit('mopidy.tracklist.set', data);
                successCallback(data);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Set tracks from URI, clearing previous tracks
     * Triggers CoreListener.tracklist_changed().
     * @param {string} uri the URI of the track
     * @param {function} successCallback returns array of tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.setUri = function(uri, successCallback, errorCallback) {
        return manager.clear(function(data) {
            return manager.addUri(uri, null, function(data) {
                io.emit('mopidy.tracklist.set', data);
                successCallback(data);
            }, errorCallback);
        }, errorCallback);
    }

    /**
     * Filter tracklist tracks based on criteria
     * @param {object} criteria dictionary of (string, list) pairs
     * @param {function} successCallback returns array of tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.filterTlTracks = function(criteria, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.filter(criteria).then(successCallback, errorCallback);
    }

    /**
     * Get index of a tracklist track
     * @param {object} tl_track tracklist track object
     * @param {function} successCallback returns integer of index
     * @param {function} errorCallback returns the error
     */
    manager.indexOfTlTrack = function(tl_track, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.index({tl_track: tl_track}).then(successCallback, errorCallback);
    }

    /**
     * Get slice from the tracklist
     * @param {int} start first index to include in slice
     * @param {int} end last index to include in slice
     * @param {function} successCallback returns array of tl_track objects
     * @param {function} errorCallback returns the error
     */
    manager.sliceTlTracks = function(start, end, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.slice({start: start, end: end}).then(successCallback, errorCallback);
    }

    /**
     * The track that will be played after the given track.
     * Not neccisarily the same track as nextTlTrack().
     * @param {object|null} tl_track reference tracklist track object
     * @param {function} successCallback returns tl_track object
     * @param {function} errorCallback returns the error
     */
    manager.getEotTlTrack = function(tl_track, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.eotTrack({tl_track: tl_track}).then(successCallback, errorCallback);
    }

    /**
     * The track that will be played if calling playbackManager.next()
     * @param {object|null} tl_track reference tracklist track object
     * @param {function} successCallback returns tl_track object
     * @param {function} errorCallback returns the error
     */
    manager.getNextTlTrack = function(tl_track, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.nextTrack({tl_track: tl_track}).then(successCallback, errorCallback);
    }

    /**
     * The track that will be played if calling playbackManager.previous()
     * @param {object|null} tl_track reference tracklist track object
     * @param {function} successCallback returns tl_track object
     * @param {function} errorCallback returns the error
     */
    manager.getPreviousTlTrack = function(tl_track, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.previousTrack({tl_track: tl_track}).then(successCallback, errorCallback);
    }

    /**
     * Get tracklist tracks from tracklist
     * @param {function} successCallback returns array of tl_tracks objects
     * @param {function} errorCallback returns the error
     */
    manager.getTlTracks = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getTlTracks().then(successCallback, errorCallback);
    }

    /**
     * Get tracks from tracklist
     * @param {function} successCallback returns array of tracks objects
     * @param {function} errorCallback returns the error
     */
    manager.getTracks = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getTracks().then(successCallback, errorCallback);
    }

    /**
     * Move tracklist tracks in the tracklist
     * Triggers CoreListener.tracklist_changed().
     * @param {int} start index of first track to move (inclusive)
     * @param {int} end index of last track to move (inclusive)
     * @param {int} to_position index to move the track to
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.moveTlTracks = function(start, end, to_position, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.move({start: start, end: end + 1, to_position: to_position}).then(function(data) {
            io.emit('mopidy.tracklist.move', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Clear the tracklist.
     * Triggers CoreListener.tracklist_changed().
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.clear = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.clear().then(function(data) {
            io.emit('mopidy.tracklist.clear', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Remove tracks from the tracklist.
     * Triggers CoreListener.tracklist_changed().
     * @param {object} criteria dictionary of (string, list) pairs
     * @param {function} successCallback returns array of tl_track objects that were removed
     * @param {function} errorCallback returns the error
     */
    manager.removeTlTracks = function(criteria, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.remove(criteria).then(function(data) {
            io.emit('mopidy.tracklist.remove', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Shuffle tracks.
     * Triggers CoreListener.tracklist_changed().
     * @param {int|null} start index of first track to shuffle (inclusive)
     * @param {int|null} end index of last track to shuffle (inclusive)
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.shuffle = function(start, end, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        if(end){
            end += 1;
        }

        return mopidy.tracklist.shuffle({start: start, end: end}).then(function(data) {
            io.emit('mopidy.tracklist.shuffle', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get if the player is set to consume tracks once they play.
     * @param {function} successCallback returns boolean
     * @param {function} errorCallback returns the error
     */
    manager.getConsume = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getConsume().then(successCallback, errorCallback);
    }

    /**
     * Set if the player should consume tracks once they play.
     * @param {boolean} consume
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.setConsume = function(consume, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.setConsume([consume]).then(function(data) {
            io.emit('mopidy.tracklist.setConsume', consume);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get if player is set to random play.
     * @param {function} successCallback returns boolean
     * @param {function} errorCallback returns the error
     */
    manager.getRandom = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getRandom().then(successCallback, errorCallback);
    }

    /**
     * Set if the player should random play.
     * @param {boolean} random
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.setRandom = function(random, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        random = (random == true ? true : false);

        return mopidy.tracklist.setRandom([random]).then(function(data) {
            io.emit('mopidy.tracklist.setRandom', random);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get if player is set to repeat play.
     * @param {function} successCallback returns boolean
     * @param {function} errorCallback returns the error
     */
    manager.getRepeat = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getRepeat().then(successCallback, errorCallback);
    }

    /**
     * Set if the player should repeat play.
     * @param {boolean} repeat
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.setRepeat = function(repeat, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        repeat = (repeat == true ? true : false);

        return mopidy.tracklist.setRepeat([repeat]).then(function(data) {
            io.emit('mopidy.tracklist.setRepeat', repeat);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get if player is set to single play.
     * @param {function} successCallback returns boolean
     * @param {function} errorCallback returns the error
     */
    manager.getSingle = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getSingle().then(successCallback, errorCallback);
    }

    /**
     * Set if the player should single play.
     * @param {boolean} single
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.setSingle = function(single, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        single = (single == true ? true : false);

        return mopidy.tracklist.setSingle([single]).then(function(data) {
            io.emit('mopidy.tracklist.setSingle', single);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Get tracklist length
     * @param {function} successCallback returns int
     * @param {function} errorCallback returns the error
     */
    manager.getLength = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getLength().then(successCallback, errorCallback);
    }

    /**
     * Get tracklist version, which is increased every time the tracklist is changed.
     * Is not reset before Mopidy is restarted.
     * @param {function} successCallback returns int
     * @param {function} errorCallback returns the error
     */
    manager.getVersion = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.tracklist.getVersion().then(successCallback, errorCallback);
    }

    return manager;
}