
module.exports = function(mopidy, io) {
    var manager = {};

    /**
     * Browse directories and tracks at the given uri.
     * @param {string} uri
     * @param {function} successCallback returns array of ref objects
     * @param {function} errorCallback returns the error
     */
    manager.browse = function(uri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.library.browse({uri: uri}).then(successCallback, errorCallback);
    }

    /**
     * Lookup given URI.
     * If the URI expands to multiple tracks, the returned list will contain them all.
     * @param {string} uri
     * @param {function} successCallback returns array of track objects
     * @param {function} errorCallback returns the error
     */
    manager.lookup = function(uri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.library.lookup({uri: uri}).then(successCallback, errorCallback);
    }

    /**
     * Search the library for tracks based on a query.
     * If the query is empty, and the backend can support it, all available tracks are returned.
     * If uris is given, the search is limited to results from within the URI roots.
     * For example passing uris=['file:'] will limit the search to the local backend.
     * (Don't know the difference between this and search()...)
     * @param {object} query will find tracks where field (key) == value
     * @param {array|null} uris list of URIs
     * @param {function} successCallback returns array of search_result objects
     * @param {function} errorCallback returns the error
     */
    manager.findExact = function(query, uris, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.library.findExact({query: query, uris: uris}).then(successCallback, errorCallback);
    }

    /**
     * Search the library for tracks based on a query.
     * If the query is empty, and the backend can support it, all available tracks are returned.
     * If uris is given, the search is limited to results from within the URI roots.
     * For example passing uris=['file:'] will limit the search to the local backend.
     * (Don't know the difference between this and findExact()...)
     * @param {object} query will find tracks where field (key) == value
     * @param {array|null} uris list of URIs
     * @param {function} successCallback returns array of search_result objects
     * @param {function} errorCallback returns the error
     */
    manager.search = function(query, uris, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.library.search({query: query, uris: uris}).then(successCallback, errorCallback);
    }

    /**
     * Refresh library.
     * @param {string|null} uri limit to URI and below if an URI is given.
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.refresh = function(uri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.library.refresh({uri: uri}).then(successCallback, errorCallback);
    }


    return manager;
}