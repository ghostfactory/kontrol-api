
module.exports = function(mopidy, io) {
    var manager = {};

    /**
     * Get supported URI schemes
     * @param {function} successCallback returns array of uri_schemes
     * @param {function} errorCallback returns the error
     */
    manager.getUriSchemes = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        return mopidy.getUriSchemes().then(successCallback, errorCallback);
    }

    return manager;
}