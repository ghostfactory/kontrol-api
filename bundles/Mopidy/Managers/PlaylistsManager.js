/* ==== App ==== */

var Playlist = requireApp('bundles/Mopidy/Models/Playlist');

/* =========================================== */


module.exports = function(mopidy, io) {
    var manager = {};

    /**
     * Create new playlist.
     * TODO: CAN'T CREATE SPOTIFY PLAYLIST
     * @param {string} name playlist name
     * @param {string|null} uri for which backend to use, finds first if null
     * @param {function} successCallback returns playlist object
     * @param {function} errorCallback returns the error
     */
    manager.create = function(name, uri_scheme, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.create({name: name, uri_scheme: uri_scheme}).then(function(data) {
            io.emit('mopidy.playlists.create', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Delete a playlist.
     * @param {string} uri
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.delete = function(uri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.delete({uri: uri}).then(function(data) {
            io.emit('mopidy.playlists.delete', data);
            successCallback(data);
        }, errorCallback);
    }

    /**
     * Filter playlists by criteria.
     * @param {object} criteria dictionary of (string, list) pairs
     * @param {function} successCallback returns array of playlist objects
     * @param {function} errorCallback returns the error
     */
    manager.filter = function(criteria, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.filter(criteria).then(function(playlistsData) {
            var playlists = [];
            for(var i in playlistsData) {
                var playlistData = playlistsData[i];
                playlists.push( new Playlist(playlistData) );
            }

            successCallback(playlists);
        }, errorCallback);
    }

    /**
     * Lookup playlist by URI.
     * @param {string} uri
     * @param {function} successCallback returns playlist object or null if not found
     * @param {function} errorCallback returns the error
     */
    manager.lookup = function(uri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.lookup({uri: uri}).then(function(playlistsData) {
            var playlists = [];
            for(var i in playlistsData) {
                var playlistData = playlistsData[i];
                playlists.push( new Playlist(playlistData) );
            }

            successCallback(playlists);
        }, errorCallback);
    }

    /**
     * Save changes to playlist.
     * For a playlist to be saveable, it must have the uri attribute set.
     * You should not set the uri atribute yourself,
     * but use playlist objects returned by create() or retrieved from playlists,
     * which will always give you saveable playlists.
     * The method returns the saved playlist.
     * The return playlist may differ from the saved playlist.
     * E.g. if the playlist name was changed, the returned playlist may have a different URI.
     * @param {object} playlist
     * @param {function} successCallback returns playlist object or null if uri not set or wrong
     * @param {function} errorCallback returns the error
     */
    manager.save = function(playlist, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.save({playlist: playlist}).then(function(playlistData) {
            var playlist = new Playlist(playlistData);

            io.emit('mopidy.playlists.save', playlist);
            successCallback(playlist);
        }, errorCallback);
    }

    /**
     * Get all playlists
     * @param {function} successCallback returns array of playlist objects
     * @param {function} errorCallback returns the error
     */
    manager.getPlaylists = function(successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.getPlaylists().then(function(playlistsData) {
            var playlists = [];
            for(var i in playlistsData) {
                var playlistData = playlistsData[i];
                var playlist = new Playlist(playlistData);
                playlists.push(playlist);
            }

            successCallback(playlists)
        }, errorCallback);
    }

    /**
     * Get single playlist
     * @param {string} playlist uri
     * @param {function} successCallback returns playlist object
     * @param {function} errorCallback returns the error
     */
    manager.getPlaylist = function(playlistUri, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.getPlaylists().then(function(playlistsData) {
            var playlistData = _.find(playlistsData, function(p) {
                return p.uri == playlistUri;
            });

            if(playlistData == null) {
                return errorCallback("Playlist not found.");
            }

            var playlist = new Playlist(playlistData);

            successCallback(playlist)
        }, errorCallback);
    }

    /**
     * Refresh playlsits.
     * If uri_scheme is null, all backends are asked to refresh.
     * @param {string|null} uri_scheme limit to the backend matching the URI scheme
     * @param {function} successCallback returns null
     * @param {function} errorCallback returns the error
     */
    manager.refresh = function(uri_scheme, successCallback, errorCallback) {
        if(!mopidy.isReady) return mopidy.notReady(errorCallback);

        mopidy.playlists.refresh({uri_scheme: uri_scheme}).then(successCallback, errorCallback);
    }


    return manager;
}