/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(mopidyProgram, server, io) 
{
    var resource = '/mopidy/library';

    /* ================================================= */
    /* GET
    /* ================================================= */

    /*
     * Browse
     */
    server.get(resource + "/browse", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri)
        }

        return mopidyProgram.library.browse(params.uri, function (refs) {
            return new Response(response, refs).success();
        }, function (error) {
            return new Response(response).error("There was an error browsing library.", error);
        });
    });

    /*
     * Lookup URI
     */
    server.get(resource + "/lookup", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri)
        }

        return mopidyProgram.library.lookup(params.uri, function (tracks) {
            return new Response(response, tracks).success();
        }, function (error) {
            return new Response(response).error("There was an error looking up info.", error);
        });
    });

    /*
     * Find exact
     */
    server.get(resource + "/find-exact", function (request, response, next) {
        if(Util.validationResponse(['query'], request, response)) return;
        var params = {
            query:  Util.params(request.params.query),
            uris:   Util.params(request.params.uris)
        }

        return mopidyProgram.library.browse(params.query, params.uris, function (search_results) {
            return new Response(response, search_results).success();
        }, function (error) {
            return new Response(response).error("There was an error finding exact info.", error);
        });
    });

    /*
     * Search
     */
    server.get(resource + "/search", function (request, response, next) {
        if(Util.validationResponse(['query'], request, response)) return;
        var params = {
            query:  Util.params(request.params.query),
            uris:   Util.params(request.params.uris)
        }

        return mopidyProgram.library.search(params.query, params.uris, function (search_results) {
            return new Response(response, search_results).success();
        }, function (error) {
            return new Response(response).error("There was an error searching.", error);
        });
    });

    /*
     * Refresh
     */
    server.get(resource + "/refresh", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri)
        }

        return mopidyProgram.library.refresh(params.uri, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error refreshing.", error);
        });
    });

}