/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(mopidyProgram, server, io)
{
    var resource = '/mopidy/core';

    /* ================================================= */
    /* POST
    /* ================================================= */




    /* ================================================= */
    /* GET
    /* ================================================= */

    /*
     * URI Schemes
     */
    server.get(resource + "/uri-schemes", function (request, response, next) {
        mopidyProgram.core.getUriSchemes(function (uri_schemes) {
            return Response(response, uri_schemes).success();
        }, function (error) {
            return Response(response).error("There was an error getting uri schemes.", error);
        });
    });

}