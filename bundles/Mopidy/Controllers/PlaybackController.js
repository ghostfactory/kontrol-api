/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

var Playlist = requireApp("bundles/Mopidy/Models/Playlist");
var Track = requireApp("bundles/Mopidy/Models/Track");

/* ==== Libs ==== */

var _ = require("underscore");
var Util = require(process.cwd() + "/lib/util");


/* ============================================================ */

module.exports = function(mopidyProgram, server, io)
{
    var resource = "/mopidy/playback";


    /* ================================================= */
    /* POST
    /* ================================================= */

    /*
     * Play URI
     */
    server.post(resource + "/play/:uri", function(request, response, next) {
        var uri = Util.params(request.params.uri);

        mopidyProgram.playback.playUri(uri, null, function(track) {
            return new Response(response, track).success();
        }, function(error) {
            return new Response(response).error("There was an error playing URI.", error);
        })
    });

    /*
     * Play URI in playlist
     */
    server.post(resource + "/play/:playlistUri/:trackUri", function(request, response, next) {
        var playlistUri = Util.params(request.params.playlistUri);
        var trackUri = Util.params(request.params.trackUri);

        mopidyProgram.playback.playUriInPlaylist(playlistUri, trackUri, function(track) {
            return new Response(response, track).success();
        }, function(error) {
            return new Response(response).error("There was an error playing URI.", error);
        })
    });

    ///*
    // * Play Tracks
    // */
    //server.post(resource + "/play/tracks", function (request, response, next) {
    //    if(Util.validationResponse(["tracks"], request, response)) return;
    //    var params = {
    //        tracks:         Util.params(request.params.tracks),
    //        at_position:    Util.params(request.params.at_position)
    //    }
    //
    //    return mopidyProgram.playback.playTracks(params.tracks, params.at_position, function (data) {
    //        return mopidyProgram.playback.getCurrentTrack(function (track) {
    //            return new Response.success(response, track);
    //        }, function (error) {
    //            return new Response.success(response, "Play successful, but could not get now playing info.", error);
    //        });
    //    }, function (error) {
    //        return new Response.error(response, "There was an error playing the tracks.", error);
    //    });
    //});
    //
    ///*
    // * Play Tracklist Track
    // */
    //server.post(resource + "/play/tl-track", function (request, response, next) {
    //    //if(Util.validationResponse(["tl_track"], request, response)) return;
    //    var params = {
    //        tl_track:       Util.params(request.params.tl_track),
    //        on_error_step:  Util.params(request.params.on_error_step)
    //    }
    //
    //    return mopidyProgram.playback.playTlTrack(params.tl_track, params.on_error_step, function (data) {
    //        return mopidyProgram.playback.getCurrentTrack(function (track) {
    //            return new Response.success(response, track);
    //        }, function (error) {
    //            return new Response.success(response, "Play successful, but could not get now playing info.", error);
    //        });
    //    }, function (error) {
    //        return new Response.error(response, "There was an error playing the tracklist track.", error);
    //    });
    //});

    /*
     * Stop playing
     */
    server.post(resource + "/stop", function (request, response, next) {
        var params = {
            clear_current_track: Util.params(request.params.clear_current_track)
        };

        mopidyProgram.playback.stop(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error stopping.", error);
        });
    });

    /*
     * Pause playing
     */
    server.post(resource + "/pause", function (request, response, next) {
        mopidyProgram.playback.pause(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error pausing.", error);
        });
    });

    /*
     * Resume playing
     */
    server.post(resource + "/resume", function (request, response, next) {
        mopidyProgram.playback.resume(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error resuming.", error);
        });
    });

    /*
     * Set time position
     */
    server.post(resource + "/seek", function (request, response, next) {
        if(Util.validationResponse(["time_position"], request, response)) return;
        var params = {
            time_position: Util.params(request.params.time_position)
        }

        mopidyProgram.playback.seek(params.time_position, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error seeking.", error);
        });
    });

    /*
     * Change TlTrack
     */
    server.post(resource + "/change/tl-track", function (request, response, next) {
        if(Util.validationResponse(["tl_track"], request, response)) return;
        var params = {
            tl_track:       Util.params(request.params.tl_track),
            on_error_step:  Util.params(request.params.on_error_step)
        };

        mopidyProgram.playback.changeTlTrack(params.tl_track, params.on_error_step, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error changing to track.", error);
        });
    });

    /*
     * Change URI
     */
    server.post(resource + "/change/uri", function (request, response, next) {
        if(Util.validationResponse(["uri"], request, response)) return;
        var params = {
            uri:            Util.params(request.params.uri),
            on_error_step:  Util.params(request.params.on_error_step)
        };

        mopidyProgram.playback.changeUri(params.uri, params.on_error_step, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error changing to track.", error);
        });
    });

    /*
     * Play next track
     */
    server.post(resource + "/play/next", function (request, response, next) {
        mopidyProgram.playback.playNextTrack(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error playing next track.", error);
        });
    });

    /*
     * Play previous track
     */
    server.post(resource + "/play/previous", function (request, response, next) {
        mopidyProgram.playback.playPreviousTrack(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error playing previous track.", error);
        });
    });

    /*
     * Set mute
     */
    server.post(resource + "/mute/", function (request, response, next) {
        if(Util.validationResponse(["mute"], request, response)) return;
        var params = {
            mute: Util.params(request.params.mute)
        }

        mopidyProgram.playback.setMute(params.mute, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error muting.", error);
        });
    });


    /* ================================================= */
    /* GET
    /* ================================================= */

    /*
     * Current track
     */
    server.get(resource + "/current-track", function (request, response, next) {
        // Get track
        mopidyProgram.playback.getCurrentTrack(function (trackData) {
            // Get time position
            mopidyProgram.playback.getTimePosition(function (timePosition) {
                var track = new Track(trackData);
                track.timePosition = timePosition;

                return new Response(response, track).success();
            }, function (error) {
                return new Response(response).error("There was an error getting time position.", error);
            });
        }, function (error) {
            return new Response(response).error("There was an error getting current track.", error);
        });
    });

    /*
     * Current tracklist track
     */
    server.get(resource + "/current-tl-track", function (request, response, next) {
        mopidyProgram.playback.getCurrentTlTrack(function (tl_track) {
            return new Response(response, tl_track).success();
        }, function (error) {
            return new Response(response).error("There was an error getting current tracklist track.", error);
        });
    });

    /*
     * Time position
     */
    server.get(resource + "/time-position", function (request, response, next) {
        mopidyProgram.playback.getTimePosition(function (time_position) {
            return new Response(response, time_position).success();
        }, function (error) {
            return new Response(response).error("There was an error getting time position.", error);
        });
    });

    /*
     * State
     */
    server.get(resource + "/state", function (request, response, next) {
        mopidyProgram.playback.getState(function (state) {
            return new Response(response, state).success();
        }, function (error) {
            return new Response(response).error("There was an error getting time position.", error);
        });
    });

    /*
     * Mute
     */
    server.get(resource + "/mute", function (request, response, next) {
        mopidyProgram.playback.getMute(function (mute) {
            return new Response(response, mute).success();
        }, function (error) {
            return new Response(response).error("There was an error getting mute.", error);
        });
    });

    /*
     * Volume
     */
    server.get(resource + "/volume", function (request, response, next) {
        mopidyProgram.playback.getVolume(function (volume) {
            return new Response(response, volume).success();
        }, function (error) {
            return new Response(response).error("There was an error getting mute.", error);
        });
    });

}