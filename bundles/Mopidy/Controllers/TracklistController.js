/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");

/* ==== Libs ==== */

var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(mopidyProgram, server, io)
{
    var resource = '/mopidy/tracklist';

    /* ================================================= */
    /* POST
    /* ================================================= */

    /*
     * Set tracks
     */
    server.post(resource + "/tracks", function (request, response, next) {
        if(Util.validationResponse(['tracks'], request, response)) return;
        var params = {
            tracks: Util.params(request.params.tracks)
        };

        mopidyProgram.tracklist.setTracks(params.tracks, function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error setting tracks.", error);
        });
    });

    /*
     * Set URI
     */
    server.post(resource + "/uri", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri)
        };

        mopidyProgram.tracklist.setUri(params.uri, function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response.error("There was an error setting tracks.", error);
        });
    });

    /*
     * Move tracklist tracks
     */
    server.post(resource + "/move", function (request, response, next) {
        if(Util.validationResponse(['start', 'end', 'to_position'], request, response)) return;
        var params = {
            start:          Util.params(request.params.start),
            end:            Util.params(request.params.end),
            to_position:    Util.params(request.params.to_position)
        };

        mopidyProgram.tracklist.moveTlTracks(params.start, params.end, params.to_position, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error moving tracks.", error);
        });
    });

    /*
     * Shuffle
     */
    server.post(resource + "/shuffle", function (request, response, next) {
        var params = {
            start:  Util.params(request.params.start),
            end:    Util.params(request.params.end)
        };

        mopidyProgram.tracklist.shuffle(params.start, params.end, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error shuffling tracks.", error);
        });
    });

    /*
     * Clear tracks
     */
    server.post(resource + "/clear", function (request, response, next) {
        mopidyProgram.tracklist.clear(function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error clearing tracklist.", error);
        });
    });

    /*
     * Remove tracks
     */
    server.post(resource + "/remove", function (request, response, next) {
        if(Util.validationResponse(['criteria'], request, response)) return;
        var params = {
            criteria: Util.params(request.params.criteria)
        };

        mopidyProgram.tracklist.removeTlTracks(params.criteria, function (removed_tl_tracks) {
            return new Response(response, removed_tl_tracks).success();
        }, function (error) {
            return new Response(response).error("There was an error removing tracklist tracks.", error);
        });
    });

    /*
     * Set consume
     */
    server.post(resource + "/consume", function (request, response, next) {
        if(Util.validationResponse(['consume'], request, response)) return;
        var params = {
            consume: Util.params(request.params.consume)
        };

        mopidyProgram.tracklist.setConsume(params.consume, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error setting consume.", error);
        });
    });

    /*
     * Set random
     */
    server.post(resource + "/random", function (request, response, next) {
        if(Util.validationResponse(['random'], request, response)) return;
        var params = {
            random: Util.params(request.params.random)
        };

        mopidyProgram.tracklist.setRandom(params.random, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error setting random.", error);
        });
    });

    /*
     * Set repeat
     */
    server.post(resource + "/repeat", function (request, response, next) {
        if(Util.validationResponse(['repeat'], request, response)) return;
        var params = {
            repeat: Util.params(request.params.repeat)
        };

        mopidyProgram.tracklist.setRepeat(params.repeat, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error setting repeat.", error);
        });
    });

    /*
     * Set single
     */
    server.post(resource + "/single", function (request, response, next) {
        if(Util.validationResponse(['single'], request, response)) return;
        var params = {
            single: Util.params(request.params.single)
        };

        mopidyProgram.tracklist.setSingle(params.single, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error setting single.", error);
        });
    });


    /* ================================================= */
    /* PUT
    /* ================================================= */

    /*
     * Add tracks
     */
    server.put(resource + "/tracks", function (request, response, next) {
        if(Util.validationResponse(['tracks'], request, response)) return;
        var params = {
            tracks: Util.params(request.params.tracks),
            at_position: Util.params(request.params.at_position)
        };

        mopidyProgram.tracklist.addTracks(params.tracks, params.at_position, function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error adding tracks.", error);
        });
    });

    /*
     * Add URI
     */
    server.put(resource + "/uri", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri),
            at_position: Util.params(request.params.at_position)
        };

        mopidyProgram.tracklist.addUri(params.uri, params.at_position, function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error adding tracks.", error);
        });
    });


    /* ================================================= */
    /* GET
    /* ================================================= */

    /*
     * Get tracks
     */
    server.get(resource, function (request, response, next) {
        mopidyProgram.tracklist.getTracks(function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error getting tracks.", error);
        });
    });

    /*
     * Get tracklist tracks
     */
    server.get(resource + "/tl_tracks", function (request, response, next) {
        mopidyProgram.tracklist.getTlTracks(function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error getting tracklist tracks.", error);
        });
    });

    /*
     * Filter tracklist
     */
    server.get(resource + "/filter", function (request, response, next) {
        if(Util.validationResponse(['criteria'], request, response)) return;
        var params = {
            criteria: Util.params(request.params.criteria)
        };

        mopidyProgram.tracklist.filterTlTracks(params.criteria, function (tracklist) {
            return new Response(response, tracklist).success();
        }, function (error) {
            return new Response(response).error("There was an error getting tracklist.", error);
        });
    });

    /*
     * Get index of tracklist track
     */
    server.get(resource + "/index_of", function (request, response, next) {
        if(Util.validationResponse(['tl_track'], request, response)) return;
        var params = {
            tl_track: Util.params(request.params.tl_track)
        };

        mopidyProgram.tracklist.indexOfTlTrack(params.tl_track, function (index) {
            return new Response(response, index).success();
        }, function (error) {
            return new Response(response).error("There was an error getting index of tracklist track.", error);
        });
    });

    /*
     * Get slice
     */
    server.get(resource + "/slice", function (request, response, next) {
        if(Util.validationResponse(['start', 'end'], request, response)) return;
        var params = {
            start: Util.params(request.params.start),
            end: Util.params(request.params.end)
        };

        mopidyProgram.tracklist.sliceTlTracks(params.start, params.end, function (slice) {
            return new Response(response, slice).success();
        }, function (error) {
            return new Response(response).error("There was an error getting slice of tracklist tracks.", error);
        });
    });

    /*
     * Get next track
     */
    server.get(resource + "/next", function (request, response, next) {
        var params = {
            tl_track: Util.params(request.params.tl_track)
        };

        mopidyProgram.tracklist.getNextTlTrack(params.tl_track, function (tl_track) {
            return new Response(response, tl_track).success();
        }, function (error) {
            return new Response(response).error("There was an error getting next tracklist track.", error);
        });
    });

    /*
     * Get previous track
     */
    server.get(resource + "/previous", function (request, response, previous) {
        var params = {
            tl_track: Util.params(request.params.tl_track)
        };

        mopidyProgram.tracklist.getPreviousTlTrack(params.tl_track, function (tl_track) {
            return new Response(response, tl_track).success();
        }, function (error) {
            return new Response(response).error("There was an error getting previous tracklist track.", error);
        });
    });

    /*
     * Get EOT track
     */
    server.get(resource + "/eot", function (request, response, next) {
        var params = {
            tl_track: Util.params(request.params.tl_track)
        };

        mopidyProgram.tracklist.getEotTlTrack(params.tl_track, function (tl_track) {
            return new Response(response, tl_track).success();
        }, function (error) {
            return new Response(response).error("There was an error getting EOT tracklist track.", error);
        });
    });

    /*
     * Get consume
     */
    server.get(resource + "/consume", function (request, response, next) {
        mopidyProgram.tracklist.getConsume(function (consume) {
            return new Response(response, consume).success();
        }, function (error) {
            return new Response(response).error("There was an error getting consume setting.", error);
        });
    });

    /*
     * Get random
     */
    server.get(resource + "/random", function (request, response, next) {
        mopidyProgram.tracklist.getRandom(function (random) {
            return new Response(response, random).success();
        }, function (error) {
            return new Response(response).error("There was an error getting random setting.", error);
        });
    });

    /*
     * Get repeat
     */
    server.get(resource + "/repeat", function (request, response, next) {
        mopidyProgram.tracklist.getRepeat(function (repeat) {
            return new Response(response, repeat).success();
        }, function (error) {
            return new Response(response).error("There was an error getting repeat setting.", error);
        });
    });

    /*
     * Get single
     */
    server.get(resource + "/single", function (request, response, next) {
        mopidyProgram.tracklist.getSingle(function (single) {
            return new Response(response, single).success();
        }, function (error) {
            return new Response(response).error("There was an error getting single setting.", error);
        });
    });

    /*
     * Get length
     */
    server.get(resource + "/length", function (request, response, next) {
        mopidyProgram.tracklist.getLength(function (length) {
            return new Response(response, length).success();
        }, function (error) {
            return new Response(response).error("There was an error getting length setting.", error);
        });
    });

    /*
     * Get version
     */
    server.get(resource + "/version", function (request, response, next) {
        mopidyProgram.tracklist.getVersion(function (version) {
            return new Response(response, version).success();
        }, function (error) {
            return new Response(response).error("There was an error getting version setting.", error);
        });
    });

}