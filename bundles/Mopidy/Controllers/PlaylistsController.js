/* ==== App ==== */

var Response = requireApp("app/Utilities/Response");
var Playlist = requireApp("bundles/Mopidy/Models/Playlist");

/* ==== Libs ==== */

var _ = require('underscore');
var Util = require(process.cwd() + '/lib/util');


/* ============================================================ */

module.exports = function(mopidyProgram, server, io)
{
    var resource = '/mopidy/playlists';

    /* ================================================= */
    /* POST
    /* ================================================= */

    /*
     * Create playlist
     */
    server.post(resource + "/new", function (request, response, next) {
        if(Util.validationResponse(['name', 'uri_scheme'], request, response)) return;
        var params = {
            name:       Util.params(request.params.name),
            uri_scheme: Util.params(request.params.uri_scheme)
        };

        mopidyProgram.playlists.create(params.name, params.uri_scheme, function (playlistData) {
            var playlist = new Playlist(playlistData);

            return new Response(response, playlist).success();
        }, function (error) {
            return new Response(response).error("There was an error creating playlist.", error);
        });
    });

    /*
     * Refresh playlist
     */
    server.post(resource + "/refresh", function (request, response, next) {
        var params = {
            uri_scheme: Util.params(request.params.uri_scheme)
        };

        mopidyProgram.playlists.refresh(params.uri_scheme, function (data) {
            return new Response(response, data).success();
        }, function (error) {
            return new Response(response).error("There was an error refreshing playlist.", error);
        });
    });


    /* ================================================= */
    /* PUT
    /* ================================================= */

    /*
     * Save playlist
     */
    server.put(resource + "/update", function (request, response, next) {
        if(Util.validationResponse(['playlist'], request, response)) return;
        var params = {
            playlist: Util.params(request.params.playlist)
        };

        mopidyProgram.playlists.save(params.playlist, function (playlist) {
            return new Response(response, playlist).success();
        }, function (error) {
            return new Response(response).error("There was an error saving playlist.", error);
        });
    });


    /* ================================================= */
    /* GET
    /* ================================================= */

    /*
     * Get all playlists
     */
    server.get(resource + "", function (request, response, next) {
        mopidyProgram.playlists.getPlaylists(function (playlists) {
            return new Response(response, playlists).success();
        }, function (error) {
            return new Response(response).error("There was an error getting playlists.", error);
        });
    });

    /*
     * Get single playlist
     */
    server.get(resource + "/:uri", function (request, response, next) {
        var uri = Util.params(request.params.uri);

        mopidyProgram.playlists.getPlaylist(uri, function (playlist) {
            return new Response(response, playlist).success();
        }, function (error) {
            return new Response(response).error("There was an error getting playlists.", error);
        });
    });

    /*
     * Filter
     */
    server.get(resource + "/filter", function (request, response, next) {
        if(Util.validationResponse(['criteria'], request, response)) return;
        var params = {
            criteria: Util.params(request.params.criteria)
        };

        mopidyProgram.playlists.filter(params.criteria, function (playlists) {
            return new Response(response, playlists).success();
        }, function (error) {
            return new Response(response).error("There was an error filtering playlists.", error);
        });
    });

    /*
     * Lookup
     */
    server.get(resource + "/lookup", function (request, response, next) {
        if(Util.validationResponse(['uri'], request, response)) return;
        var params = {
            uri: Util.params(request.params.uri)
        };

        mopidyProgram.playlists.lookup(params.uri, function (playlists) {
            return new Response(response, playlists).success();
        }, function (error) {
            return new Response(response).error("There was an error looking up playlist.", error);
        });
    });


    /* ================================================= */
    /* DELETE
    /* ================================================= */

    /*
     * Delete playlist
     */
    //server.post(resource + "/delete", function (request, response, next) {
    //    var params = {
    //        uri: request.params.uri
    //    };
    //
    //    mopidyProgram.playlists.delete(params.uri, function (data) {
    //        return new Response(response, data).success();
    //    }, function (error) {
    //        return new Response(response).error("There was an error deleting playlist.", error);
    //    });
    //});

}