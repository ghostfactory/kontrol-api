/* ==== App ==== */

var CONFIG = requireApp("app/config/config");
var ProcessChecker = requireApp("app/Utilities/ProcessChecker");
var Response = requireApp("app/Utilities/Response");


/* ============================================================ */

module.exports = function(server, io)
{
    var resource = '/catbot';
    var processChecker = new ProcessChecker();

    /* ================================================= *
     * POST
     * ================================================= */




    /* ================================================= *
     * GET
     * ================================================= */

    /*
     * ProcessStatus
     */
    server.get(resource + "/status", function (request, response, next) {
        var isProcessRunning = processChecker.isExactProcessRunning(CONFIG.catBot.processName);

        return new Response(response, {
            running: isProcessRunning,
            status: (isProcessRunning ? "active" : "inactive")

        }).success();
    });

}